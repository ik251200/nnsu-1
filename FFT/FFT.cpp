#pragma GCC optimize("O3")
//#pragma GCC target("avx")
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <cmath>
#include <vector>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <queue>
#include <ctime>
#include <cassert>
#include <complex>
#include <string>
#include <cstring>
#include <chrono>
#include <random>
#include <bitset>
#include <iomanip>
#include <list>
#include <stack>
using namespace std;

using cd = complex<double>;
const double PI = 4 * atan(1.);
 
const int LOG = 21;
const int N = (1 << LOG);
const int NN = N + 5;
cd w[NN];
int rev[NN];
 
void initFFT() {
    for (int i = 0; i < N; i++)
        w[i] = cd(cos(2 * PI * i / N), sin(2 * PI * i / N));
    for (int mask = 1; mask < N; mask++)
        rev[mask] = (rev[mask >> 1] >> 1) ^ ((mask & 1) << (LOG - 1));
}
 
void FFT(cd *F) {
    for (int i = 0; i < N; i++)
        if (i < rev[i])
            swap(F[i], F[rev[i]]);
    for (int lvl = 0; lvl < LOG; lvl++) {
        int len = 1 << lvl;
        for (int st = 0; st < N; st += (len << 1))
            for (int i = 0; i < len; i++) {
                cd x = F[st + i], y = F[st + len + i] * w[i << (LOG - 1 - lvl)];
                F[st + i] = x + y;
                F[st + len + i] = x - y;
            }
    }
}
 
ll myRound(double x) {
    return (ll)(x + 0.5);
}

int main() {
	return 0;
}
