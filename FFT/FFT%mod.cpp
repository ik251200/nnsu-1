const int mod = 786433;
const int deg = 18;
const int P = 5; 
P - порождающий, который по этому модулю порождает 
группу ровно из 2^deg элементов
int roots[1 << deg];
vector<int> rev[deg + 1];
void fft(vector<int>& a, int p, int inv = 0) {
    int n = 1 << p;
    for (int i = 0; i < n; i++)
        if (rev[p][i] > i)
            swap(a[i], a[rev[p][i]]);
    for (int x = 0; (1 << x) < n; x++) {
        int len = 1 << x;
        for (int i = 0; i < n; i += 2 * len) {
            for (int j = i; j < i + len; j++) {
                auto u = a[j];
                auto v = (ll) a[j + len] * roots[(j - i) * (1 << (deg - x - 1))] % mod;
                a[j] = (u + v) % mod;
                a[j + len] = (mod + u - v) % mod;
            }
        }
    }
    if (inv) {
        reverse(a.begin() + 1, a.end());
        ll rev_n = power(n);
        for (int i = 0; i < n; i++)
            a[i] = (ll) a[i] * rev_n % mod;
    }
}
 
vector<int> mult(vector<int>& a, 
vector<int>& b) {
    int p = 0;
    while ((1 << p) < sz(a) + sz(b)) p++;
    int n = 1 << p;
    vector<int> fa(n), fb(n);
    for (int i = 0; i < sz(a); i++) fa[i] = a[i];
    for (int i = 0; i < sz(b); i++) fb[i] = b[i];
    fft(fa, p); fft(fb, p);
    for (int i = 0; i < n; i++) fa[i] = (ll) fa[i] * fb[i] % mod;
    fft(fa, p, 1);
    return fa;
} 
void init() {
    roots[0] = 1;
    for (int i = 1; i < (1 << deg); i++) {
        roots[i] = (ll) roots[i - 1] * P % mod;
    }
    for (int p = 0; p <= deg; p++) {
        rev[p].resize(1 << p);
        for (int i = 0; i < (1 << p); i++)
            rev[p][i] = (rev[p][i >> 1] >> 1) | ((i & 1) << (p - 1));           
    }
}
