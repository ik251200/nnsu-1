ll cr(vector<pair<ll,ll> >& v) {
    ll A = 0, P = 1;
    for (auto [p, r] : v) {
        auto cur = rev(P, p);
        auto rr = (p + (r - A) % p) % p;
        auto x = cur * rr % p;
        A = A + x * P;
        P *= p;
    }
    return A;
}
