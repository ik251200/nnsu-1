struct rib
{
    int b,u;
    ll f;
    int back;
};
vector<vector<rib> > g;
int n,gtime=1,s,t;
ll inf=(ll)1e18;
void add_rib(int a,int b,int u)
{
    rib r1 = { b, u, 0, g[b].size()};
    rib r2 = { a, 0, 0, g[a].size()};
//if (g:(v,e)=(e,v)) u=u; else u=0;
    g[a].push_back (r1);
    g[b].push_back (r2);
}
vector<int> used;
vector<int> nex;
bool dfs(int s,int t)
{
    if (s==t) re 1;
    used[s]=gtime;
    int i;
    fo(i,g[s].size())
    {
        if (g[s][i].u!=g[s][i].f&&used[g[s][i].b]!=gtime)
        {
            if (dfs(g[s][i].b,t))
            {
                nex[s]=i;
                re 1;
            }
        }
    }
    re 0;
}
void flow(int s,int t)
{
    while(dfs(s,t))
    {
        gtime++;
        int t1,t2;
        t1=s;
        ll d=inf;
        while(t1!=t)
        {
            t2=nex[t1];
            d=min(d,(ll)g[t1][t2].u-g[t1][t2].f);
            t1=g[t1][t2].b;
        }
        t1=s;
        while(t1!=t)
        {
            t2=nex[t1];
            g[t1][t2].f+=d;
            g[g[t1][t2].b][g[t1][t2].back].f-=d;
            t1=g[t1][t2].b;
        }
    }
}
void prepare()
{
    used.assign(n,0);
    nex.assign(n,0);
    gtime=1;
    flow(s,t);
}
ll otv()
{
    ll o=0;
    int i;
    fo(i,g[s].size())
    {
        o+=g[s][i].f;
    }
    re o;
}
