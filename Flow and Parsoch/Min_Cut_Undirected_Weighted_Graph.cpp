int mincut(int n,vector<vector<int> > &g,vector<int> &best_cut)
{
    int best_cost = inf;
	vector<vector<int> > v(n);
	for (int i=0; i<n; ++i)
		v[i].assign (1, i);
	vector<int> w(n);
	vector<bool> exist(n,1), in_a(n);
	for (int ph=0; ph<n-1; ++ph) {
		in_a.assign(n,0);
		w.assign(n,0);
		for (int it=0, prev; it<n-ph; ++it) {
			int sel = -1;
			for (int i=0; i<n; ++i)
				if (exist[i] && !in_a[i] && (sel == -1 || w[i] > w[sel]))
					sel = i;
			if (it == n-ph-1) {
				if (w[sel] < best_cost)
					best_cost = w[sel],  best_cut = v[sel];
				v[prev].insert (v[prev].end(), v[sel].begin(), v[sel].end());
				for (int i=0; i<n; ++i)
					g[prev][i] = g[i][prev] += g[sel][i];
				exist[sel] = false;
			}
			else {
				in_a[sel] = true;
				for (int i=0; i<n; ++i)
					w[i] += g[sel][i];
				prev = sel;
			}
		}
	}
	re best_cost;
}
